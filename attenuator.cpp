#include "attenuator.h"

Attenuator::Attenuator(QString serialNumber, int deviceIndex, QObject *parent) : QObject(parent),
    dir(Direction::down),
    minValue(0),
    maxValue(0),
    curValue(0),
    codeFreqDiap(0),
    responseCode(0),
    connectionState(false),
    serialNumber(serialNumber),
    deviceIndex(deviceIndex)
{}

void Attenuator::setSpeed(double speed)
{
        sBs1Distance = sBs1Distance * 1000;
        speed = (speed*1000)/3600;
        startBs1 = sBs1Distance / speed;
        startBs1 *= 1000;
        startBs1 /= 20;

        bs1Bs2DistanceNormal = bs1Bs2Distance;
        bs1Bs2DistanceNormal = bs1Bs2DistanceNormal * 1000;
        bs1Bs2Distance = (bs1Bs2Distance-0.4) * 1000;
        bs1bs2Normal = bs1Bs2DistanceNormal / speed;
        bs1Bs2 = bs1Bs2Distance / speed;
        bs1bs2Normal *= 1000;
        bs1bs2Normal /= 20;
        bs1Bs2 *= 1000;
        bs1Bs2 /= 20;



        bs2Bs3Distance = bs2Bs3Distance * 1000;
        bs2Bs3 = bs2Bs3Distance / speed;
        bs2Bs3 *= 1000;
        bs2Bs3 /= 20;

        bs3bs4Distance = bs3bs4Distance * 1000;
        bs3Bs4 = bs3bs4Distance / speed;
        bs3Bs4 *= 1000;
        bs3Bs4 /= 20;


}



void Attenuator::connectToAttenuator()
{
    HID_UART_STATUS	status = HID_UART_DEVICE_NOT_FOUND;
    status = HidUart_Open(&devHandle, deviceIndex, VID, PID);
    if (status == HID_UART_SUCCESS)
        status = HidUart_SetUartConfig(devHandle, baudRate, dataBits, parity, stopBits, flowControl);
    if (status == HID_UART_SUCCESS) {
        DWORD vBaudRate;
        BYTE vDataBits, vParity, vStopBits, vFlowControl;
        status = HidUart_GetUartConfig(devHandle, &vBaudRate, &vDataBits, &vParity, &vStopBits, &vFlowControl);
        if (status == HID_UART_SUCCESS)
            if (vBaudRate != baudRate || vDataBits != dataBits || vParity != parity || vStopBits != stopBits || vFlowControl != flowControl)
                status = HID_UART_INVALID_PARAMETER;
   }
   if (status == HID_UART_SUCCESS)
       status = HidUart_SetTimeouts(devHandle, 0, 2000);
   if (status == HID_UART_SUCCESS) {
        emit connected(serialNumber);
        connectionState = true;
        getMinMaxDb(40, 80);
   }

   DWORD numBytesWritten = 0;
   BYTE buffer[6]	 = {0x3a, 0x00, 0x57, 0x48, 0x4f, 0x23};
   DWORD numBytesToWrite = sizeof(buffer);
   HidUart_Write(devHandle, buffer, numBytesToWrite, &numBytesWritten);
   DWORD numBytesRead	= 0;
   DWORD numBytesToRead = 1000;
   BYTE* received	= new BYTE[numBytesToRead];
   Sleep(50);
   status = HidUart_Read(devHandle, received, numBytesToRead, &numBytesRead);
   devAddrByte = received[0];
   if (serialNumber == "00536030")
        writeCommandMinMax(devAddrByte, 0x01, 2);
   else
       writeCommandMinMax(devAddrByte, 0x01, 1);
   Sleep(50);
   writeCommandMinMax(devAddrByte, 0x02, 1);
}

void Attenuator::startAttenuators()
{
    lastByte1_1st = 54;
    lastByte2_1st = 48;

    lastByte1_2nd = 56;
    lastByte2_2nd = 48;

    if (ifFirstEnd)
        delete firstEndTimer;
    firstChannelTimer = new QTimer(this);
    firstChannelTimer->setInterval(startBs1);
    connect(firstChannelTimer, &QTimer::timeout, this, &Attenuator::escalateFirstChannel);
    firstChannelTimer->start();

    secondChannelTimer = new QTimer(this);
    secondChannelTimer->setInterval(bs1bs2Normal);
    connect(secondChannelTimer, &QTimer::timeout, this, &Attenuator::escalateSecondChannel);
    ifFirst = true;
}

void Attenuator::startSecondAttenuator()
{
    lastByte1_3rd = lastByte1_4th =  56;
    lastByte2_3rd = lastByte2_4th = 48;

    thirdChannelTimer = new QTimer(this);
    thirdChannelTimer->setInterval(bs1Bs2);
    connect(thirdChannelTimer, &QTimer::timeout, this, &Attenuator::escalateThirdChannel);
    thirdChannelTimer->start();


    forthChannelTimer = new QTimer(this);
    forthChannelTimer->setInterval(bs3Bs4);
    connect(forthChannelTimer, &QTimer::timeout, this, &Attenuator::escalateForthChannel);

    ifSecond = true;
}

void Attenuator::escalateFirstChannel()
{
    if ((lastByte1_1st == 52) && (lastByte2_1st == 48) && (dir == Direction::down))  {
        writeCommandMinMax(devAddrByte, 0x01, 1);
        disconnect(firstChannelTimer, &QTimer::timeout, this, &Attenuator::escalateFirstChannel);
        firstChannelTimer->stop();
        secondChannelTimer->start();
        return;
    }
    else {
        reduceAtt(1);
        reduceAtt(2);
    }
    writeCommand(devAddrByte, 0x01);
    Sleep(50);
    writeCommand(devAddrByte, 0x02);

}

void Attenuator::escalateSecondChannel()
{
    if ((lastByte1_2nd == 52) && (lastByte2_2nd == 48) && (dir == Direction::down))  {
        writeCommandMinMax(devAddrByte, 0x02, 1);
        disconnect(secondChannelTimer, &QTimer::timeout, this, &Attenuator::escalateSecondChannel);
        secondChannelTimer->stop();
        if2ndStopped = true;
        delete firstChannelTimer;
    }
    else
        reduceAtt(2);
    writeCommand(devAddrByte, 0x02);
}

void Attenuator::escalateThirdChannel()
{
    // if reached 2nd BS (60 db) start to use third timer for both 3nd and 4th channels
    if ((lastByte1_3rd == 54) && (lastByte2_3rd == 48) && (dir == Direction::down)) {
        ifStarted4th = true;
        disconnect(thirdChannelTimer, &QTimer::timeout, this, &Attenuator::escalateThirdChannel);
        thirdChannelTimer->stop();
//        reduceAtt(3);
//        reduceAtt(4);
//        writeCommandThird(devAddrByte, 0x01);
//        Sleep(50);
//        writeCommandThird(devAddrByte, 0x02);
    }
    else if ((lastByte1_3rd == 52) && (lastByte2_3rd == 48) && (dir == Direction::down)) {
          writeCommandMinMax(devAddrByte, 0x01, 1);
          thirdChannelTimer->stop();
          // continue escalate 4th channel with separate timer
          forthChannelTimer->start();
    }
    // continue using 3rd timer for 3rd and 4th channels
    else if (ifStarted4th){
        reduceAtt(3);
        reduceAtt(4);
    }
    else
        reduceAtt(3);

    // decide write in both or only 3rd channel
    if (ifStarted4th) {
        writeCommandThird(devAddrByte, 0x01);
        Sleep(50);
        writeCommandThird(devAddrByte, 0x02);
    }
    else
      writeCommandThird(devAddrByte, 0x01);
}

void Attenuator::escalateForthChannel()
{
    if ((lastByte1_4th == 52) && (lastByte2_4th == 48) && (dir == Direction::down)) {
            writeCommandMinMax(devAddrByte, 0x02, 1);
            forthChannelTimer->stop();
            delete thirdChannelTimer;
            if4thStopped = true;
    }
    else
        reduceAtt(4);
    writeCommandThird(devAddrByte, 0x02);
}

void Attenuator::startFirstChannelEnd()
{
    // escalate from 80 db to 60 db first channel
    lastByte1_1st = 56;
    lastByte2_1st = 48;


    firstEndTimer = new QTimer(this);
    ifFirstEnd = true;
    firstEndTimer->setInterval(bs3Bs4);
    connect(firstEndTimer, &QTimer::timeout, this, &Attenuator::escalateFirstEndSlot);
    firstEndTimer->start();
}

void Attenuator::startThirdChannelAgain()
{
    lastByte1_3rd = 54;
    lastByte2_3rd = 48;

    lastByte1_4th =  56;
    lastByte2_4th = 48;

    delete thirdChannelTimer;
    thirdChannelTimer = new QTimer(this);
    thirdChannelTimer->setInterval(bs2Bs3);
    connect(thirdChannelTimer, &QTimer::timeout, this, &Attenuator::escalateThirdChannelAgain);
    thirdChannelTimer->start();
}

void Attenuator::escalateFirstEndSlot()
{
    if ((lastByte1_1st == 54) && (lastByte2_1st == 48) && (dir == Direction::down))  {
        disconnect(firstEndTimer, &QTimer::timeout, this, &Attenuator::escalateFirstEndSlot);
        firstEndTimer->stop();
        return;
    }
    else
        reduceAtt(1);
    writeCommand(devAddrByte, 0x01);
}

void Attenuator::escalateThirdChannelAgain()
{
    if ((lastByte1_3rd == 52) && (lastByte2_3rd == 48) && (dir == Direction::down)) {
          writeCommandMinMax(devAddrByte, 0x01, 1);
          thirdChannelTimer->stop();
          // continue escalate 4th channel with separate timer
          forthChannelTimer->start();
    }
    // continue using 3rd timer for 3rd and 4th channels
    else if (ifStarted4th){
        reduceAtt(3);
        reduceAtt(4);
    }

    // decide write in both or only 3rd channel
    if (ifStarted4th) {
        writeCommandThird(devAddrByte, 0x01);
        Sleep(50);
        writeCommandThird(devAddrByte, 0x02);
    }
}

void Attenuator::reduceAtt(int which)
{
    if (which ==1) {
        if ((lastByte2_1st & 0x0F) == 0) {
            lastByte1_1st -= 0x01;
            lastByte2_1st += 0x09;
        }
        else
            lastByte2_1st -= 0x01;
    }

    else if (which == 2) {
        if ((lastByte2_2nd & 0x0F) == 0) {
            lastByte1_2nd -= 0x01;
            lastByte2_2nd += 0x09;
        }
        else
            lastByte2_2nd -= 0x01;
    }

    else if (which == 3) {
        if ((lastByte2_3rd & 0x0F) == 0) {
            lastByte1_3rd -= 0x01;
            lastByte2_3rd += 0x09;
        }
        else
            lastByte2_3rd -= 0x01;
    }

    else {
        if ((lastByte2_4th & 0x0F) == 0) {
            lastByte1_4th -= 0x01;
            lastByte2_4th += 0x09;
        }
        else
            lastByte2_4th -= 0x01;
    }

}

void Attenuator::writeCommand(BYTE devAddr, BYTE channelNum)
{
    if (channelNum == 0x02) {
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, lastByte1_2nd, lastByte2_2nd, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
        if (if2ndStopped) {
            delete secondChannelTimer;
            if2ndStopped = false;
        }
    }
    else  {
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, lastByte1_1st, lastByte2_1st, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }
}

void Attenuator::writeCommandThird(BYTE devAddr, BYTE channelNum)
{
    if (channelNum == 0x01) {
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, lastByte1_3rd, lastByte2_3rd, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }
    else  {
        if (if4thStopped) {
            delete forthChannelTimer;
            if4thStopped = false;
            ifStarted4th = false;
        }
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, lastByte1_4th, lastByte2_4th, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }
}

// 0 - min, 1 - max, 2 - for first channel of first attenuator
void Attenuator::writeCommandMinMax(BYTE devAddr, BYTE channelNum, int minmax)
{
    if (minmax == 0) {
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, minByte1, minByte2, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }
    else if (minmax == 1){
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, maxByte1, maxByte2, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }
    else {
        BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, 54, 48, 0x23};
        DWORD numBytesToWrite = sizeof(channel);
        DWORD numBytesWritten = 0;
        HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
    }

}

void Attenuator::getMinMaxDb(int min, int max)
{
    minValue = min;
    lastByte1_2nd = lastByte2_2nd = 0x30;
    min = min / 10 % 10;
    lastByte1_2nd += min;
    min = minValue % 10;
    lastByte2_2nd += min;
    minByte1 = lastByte1_2nd;
    minByte2 = lastByte2_2nd;

    maxValue = max;
    maxByte1 = maxByte2 = 0x30;
    max = max / 10 % 10;
    maxByte1 += max;
    max = maxValue % 10;
    maxByte2 += max;

    lastByte1_2nd = maxByte1;
    lastByte2_2nd = maxByte2;

}

void Attenuator::pause()
{
    if (ifFirst) {
        if (firstChannelTimer->isActive())
            firstChannelTimer->stop();
        if (secondChannelTimer->isActive())
            secondChannelTimer->stop();
        if (ifFirstEnd)
            if (firstEndTimer->isActive())
                firstEndTimer->stop();
    }

    if (ifSecond) {
        if (thirdChannelTimer->isActive())
            thirdChannelTimer->stop();
        if (forthChannelTimer->isActive())
            forthChannelTimer->stop();
    }


}
