#ifndef CONFIG_H
#define CONFIG_H

#include <QObject>
#include <QSettings>
#include <QFileInfo>


class Config : public QObject
{
    Q_OBJECT
    QSettings *cnf;
    QString deltaDistance, deltaTrain;

public:
    explicit Config(QObject *parent = 0);

    void saveConfig() {
        cnf->setValue("delta_distance", deltaDistance);
        cnf->setValue("delta_train", deltaTrain);
    }
    QString getDeltaDistance();
    QString getDeltaTrain();

signals:

public slots:
};

#endif // CONFIG_H
