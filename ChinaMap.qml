import QtQuick 2.4
import QtQuick.Window 2.2
import QtPositioning 5.5
import QtLocation 5.6
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

Window {
    width: 800
    height: 500
    visible: true

    property variant topLeft: QtPositioning.coordinate(/*39.636498*/ 39.623527, /*116.777127*/116.782081)
    property variant bottomRight: QtPositioning.coordinate(/*39.563490*/ 39.550527, /*116.883888*/116.895762)
    property variant viewMap: QtPositioning.rectangle(topLeft, bottomRight)

   // property variant start_point: QtPositioning.coordinate(39.634074, 116.817308)
    property variant start_point: QtPositioning.coordinate(39.621114, 116.825481)
    property variant bs_1: QtPositioning.coordinate(39.605642, 116.835279)
    property variant bs_2: QtPositioning.coordinate(39.589060, 116.845471)
    property variant bs_3: QtPositioning.coordinate(39.572992, 116.855406)
    property variant bs_4: QtPositioning.coordinate(39.556824, 116.865356)

    property real latitude: 0
    property real longitude: 0
    property real distance: 7.92336
    property real time: 0
    property int trainSpeed: 0

   ColumnLayout {
           anchors.fill: parent
           Layout.preferredWidth: 200
           Layout.preferredHeight: 200

   Map {
                   id: mapOfChina
                   anchors.centerIn: parent;
                   anchors.fill: parent
                   plugin: Plugin {
                       name: "osm"
                   }

                   Rectangle {
                        Column {
                            TextField {
                                id: speedInput
                                placeholderText: qsTr("Train speed")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 30
                            }
                            TextField {
                                id: latitudeInput
                                placeholderText: qsTr("Latitude")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: longitudeInput
                                placeholderText: qsTr("Longitude")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: currentBS
                                placeholderText: qsTr("Current BS")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: distanceLeft
                                placeholderText: qsTr("DistanceLeft")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: uartFirst
                                placeholderText: qsTr("FirstUART")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: uartSecond
                                placeholderText: qsTr("SecondUART")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: signalQualityBs1
                                placeholderText: qsTr("SignalQualityBs1")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: signalQualityBs2
                                placeholderText: qsTr("SignalQualityBs2")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: signalQualityBs3
                                placeholderText: qsTr("SignalQualityBs3")
                            }
                            Rectangle {
                              color: "transparent"
                              width: 1
                              height: 15
                            }
                            TextField {
                                id: signalQualityBs4
                                placeholderText: qsTr("SignalQualityBs4")
                            }

                            Button {

                                text: "Start"
                                onClicked: {
                                    trainSpeed = parseInt(speedInput.text);
                                    if (time == 0)
                                        mapOfChina.countRunningTime(trainSpeed);
                                    trainAnimation.rotationDirection = 0
                                    trainAnimation.start();
                                    Train.finishFlag = 1;
                               }
                            }
                            Button   {
                                text: "Stop"
                                onClicked: {
                                    Train.doPause();
                               }
                            }
                      }
                  }

                   MapCircle {
                           center: start_point
                           radius: 30.0
                           color: 'yellow'
                           border.width: 3
                   }

                   MapCircle {
                           center: bs_1
                           radius: 30.0
                           color: 'yellow'
                           border.width: 3
                   }

                   MapCircle {
                           center: bs_2
                           radius: 30.0
                           color: 'yellow'
                           border.width: 3
                   }

                   MapCircle {
                           center: bs_3
                           radius: 30.0
                           color: 'yellow'
                           border.width: 3
                   }
                   MapCircle {
                           center: bs_4
                           radius: 30.0
                           color: 'yellow'
                           border.width: 3
                   }

                   Timer {
                       id: timer
                       interval: 50
                       repeat: true
                       running: true;
                       onTriggered: {
                            if (trainAnimation.running || Train.isRunning()) {
                                mapOfChina.setCoords(qmlTrain.coordinate);
                                mapOfChina.setCurrentBs();
                                mapOfChina.setDistance();
                                mapOfChina.setUART();
                                mapOfChina.setSignalQuality();
                           }
                         }
                   }

                   TrainMarker {
                       id: qmlTrain
                       trainName: "China-Train"
                       coordinate: Train.position
                       SequentialAnimation {
                           id: trainAnimation
                           property real rotationDirection : 0;
                           NumberAnimation {
                               target: qmlTrain; property: "bearing"; duration: 1000
                               id: numberAnimationTrain
                               easing.type: Easing.InOutQuad
                               to: trainAnimation.rotationDirection
                           }
                           ScriptAction { script: Train.startTrain() }
                       }

                       Component.onCompleted: {
                           Train.position = start_point;
                           Train.to = bs_4;
                           Train.from = start_point;
                           Train.arrived.connect(arrived)
                       }

                       function arrived() {
                           Train.finishFlag = 2;
                           Train.position = start_point;
                           Train.to = bs_4;
                           Train.from = start_point;
                           Train.startTrain();
                           trainAnimation.start();
                           Train.arrived.connect(arrived)
                       }
                   }
                   visibleRegion: viewMap


                   function setCoords(coords) {
                        latitude = coords.latitude;
                       longitude = coords.longitude
                       latitudeInput.text = latitude;
                       longitudeInput.text = longitude;
                       Train.calcPos = coords;
                   }

                   function setCurrentBs() {
                        currentBS.text = Train.currentBS();
                   }

                   function setDistance() {
                       distanceLeft.text = Train.distanceLeft();
                   }

                   function setUART() {
                       if (Train.uarts()[0] === undefined && Train.uarts()[1] === undefined) {
                           uartFirst.text = "not given";
                           uartSecond.text = "not given";
                       }
                       else if (Train.uarts()[0] === undefined) {
                           uartFirst.text = "not given";
                           uartSecond.text = Train.uarts()[1];
                       }
                       else if (Train.uarts()[1] === undefined){
                           uartFirst.text = Train.uarts()[0];
                           uartSecond.text =  "not given";
                       }
                       else {
                           uartFirst.text = Train.uarts()[0];
                           uartSecond.text = Train.uarts()[1];
                       }

                   }
                   function setSignalQuality () {
                       signalQualityBs1.text = Train.quality1();
                       signalQualityBs2.text = Train.quality2();
                       signalQualityBs3.text = Train.quality3();
                       signalQualityBs4.text = Train.quality4();
                   }

                   function countRunningTime(speedTrain) {
                        distance = distance * 1000
                        Train.speed = speedTrain;
                        speedTrain = (speedTrain*1000)/3600
                        time = distance / speedTrain;
                        time *= 1000;
                        Train.duration = time;
                   }
               }
   }
}
