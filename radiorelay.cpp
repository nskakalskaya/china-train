#include "radiorelay.h"
#include <QtConcurrent/QtConcurrent>

RadioRelay::RadioRelay(QObject *parent) : SNMPManagement(parent)
{
    numPort = 161;
    readCommunityName = "public";
    writeCommunityName = "private";
}

void RadioRelay::connectToSnmpTrF(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else
        emit isSNMPConnected(ipaddr, false);

}

void RadioRelay::connectToSnmpTrB(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else
        emit isSNMPConnected(ipaddr, false);

}

void RadioRelay::connectToSnmpBsA(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else {
        emit isSNMPConnected(ipaddr, false);
    }
}

void RadioRelay::connectToSnmpBsB(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else {
        emit isSNMPConnected(ipaddr, false);
    }
}

void RadioRelay::connectToSnmpBsC(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else {
        emit isSNMPConnected(ipaddr, false);
    }
}

void RadioRelay::connectToSnmpBsD(QString ipaddr)
{
    start();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(numPort);
    if (initializeSnmp() && ifConnected()) {
        emit isSNMPConnected(ipaddr, true);
    }
    else {
        emit isSNMPConnected(ipaddr, false);
    }
}

void RadioRelay::sendSNMPRequestsIntTrF(QString OID, int value)
{
   setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintTrF(QString OID, unsigned long value)
{
   setSnmpUnsigned(OID, value);
}

void RadioRelay::sendSNMPRequestsIntTrB(QString OID, int value)
{
    setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintTrB(QString OID, unsigned long value)
{
    setSnmpUnsigned(OID, value);
}

void RadioRelay::sendSNMPRequestsIntBsA(QString OID, int value)
{
    setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintBsA(QString OID, unsigned long value)
{
    setSnmpUnsigned(OID, value);
}

void RadioRelay::sendRequestA(QString OID)
{

    QString res = getSnmp(OID);
    emit sendBackSignalQuality(res, 1);
}

void RadioRelay::sendSNMPRequestsIntBsB(QString OID, int value)
{
    setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintBsB(QString OID, unsigned long value)
{
    setSnmpUnsigned(OID, value);
}

void RadioRelay::sendRequestB(QString OID)
{
    QString res = getSnmp(OID);
    emit sendBackSignalQuality(res, 2);
}

void RadioRelay::sendSNMPRequestsIntBsC(QString OID, int value)
{
    setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintBsC(QString OID, unsigned long value)
{
    setSnmpUnsigned(OID, value);

}

void RadioRelay::sendRequestC(QString OID)
{
    QString res = getSnmp(OID);
    emit sendBackSignalQuality(res, 3);
}

void RadioRelay::sendSNMPRequestsIntBsD(QString OID, int value)
{
    setSnmp(OID, value);
}

void RadioRelay::sendSNMPRequestsUintBsD(QString OID, unsigned long value)
{
    setSnmpUnsigned(OID, value);
}

void RadioRelay::sendRequestD(QString OID)
{
    QString res = getSnmp(OID);
    emit sendBackSignalQuality(res, 4);
}

bool RadioRelay::ifConnected()
{
    QString connect = getSnmp("1.3.6.1.4.1.50866.1.1.4.0");
    return (connect == "not used");
}





