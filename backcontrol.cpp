#include "backcontrol.h"

BackControl::BackControl(bool ifTrFront, bool ifTrBack, bool ifBs1, bool ifBs2, bool ifBs3, bool ifBs4, QObject *parent) :
ifTrFront_{ifTrFront}, ifTrBack_{ifTrBack}, ifBs1_{ifBs1}, ifBs2_{ifBs2}, ifBs3_{ifBs3}, ifBs4_{ifBs4}, QObject(parent)
{
   for (int i = 0; i < nRelays; i++) {
        radioRelay.push_back(new RadioRelay());
        radioRelay[i]->moveToThread(&radioThread[i]);
        connect(&radioThread[i], SIGNAL(started()), radioRelay[i], SLOT(start()));
        connect(&radioThread[i], SIGNAL(finished()), radioRelay[i], SLOT(deleteLater()));
        connect(radioRelay[i], &RadioRelay::isSNMPConnected, this, &BackControl::ifConnected);
        connect(radioRelay[i], &RadioRelay::sendBackSignalQuality, this, &BackControl::sendSignalQuality);
        radioThread[i].start();
   }
   connect(this, &BackControl::signalConnectTrF, radioRelay[0], &RadioRelay::connectToSnmpTrF);
   connect(this, &BackControl::signalConnectTrB, radioRelay[1], &RadioRelay::connectToSnmpTrB);
   connect(this, &BackControl::signalConnectBsA, radioRelay[2], &RadioRelay::connectToSnmpBsA);
   connect(this, &BackControl::signalConnectBsB, radioRelay[3], &RadioRelay::connectToSnmpBsB);
   connect(this, &BackControl::signalConnectBsC, radioRelay[4], &RadioRelay::connectToSnmpBsC);
   connect(this, &BackControl::signalConnectBsD, radioRelay[5], &RadioRelay::connectToSnmpBsD);

   connect(this, &BackControl::sendSNMPRequestIntTrF, radioRelay[0], &RadioRelay::sendSNMPRequestsIntTrF);
   connect(this, &BackControl::sendSNMPRequestUintTrF, radioRelay[0], &RadioRelay::sendSNMPRequestsUintTrF);

   connect(this, &BackControl::sendSNMPRequestIntTrB, radioRelay[1], &RadioRelay::sendSNMPRequestsIntTrB);
   connect(this, &BackControl::sendSNMPRequestUintTrB, radioRelay[1], &RadioRelay::sendSNMPRequestsUintTrB);

   connect(this, &BackControl::sendSNMPRequestIntBsA, radioRelay[2], &RadioRelay::sendSNMPRequestsIntBsA);
   connect(this, &BackControl::sendSNMPRequestUintBsA, radioRelay[2], &RadioRelay::sendSNMPRequestsUintBsA);
   connect(this, &BackControl::sendSignalRequestBsA, radioRelay[2], &RadioRelay::sendRequestA);

   connect(this, &BackControl::sendSNMPRequestIntBsB, radioRelay[3], &RadioRelay::sendSNMPRequestsIntBsB);
   connect(this, &BackControl::sendSNMPRequestUintBsB, radioRelay[3], &RadioRelay::sendSNMPRequestsUintBsB);
   connect(this, &BackControl::sendSignalRequestBsB, radioRelay[3], &RadioRelay::sendRequestB);

   connect(this, &BackControl::sendSNMPRequestIntBsC, radioRelay[4], &RadioRelay::sendSNMPRequestsIntBsC);
   connect(this, &BackControl::sendSNMPRequestUintBsC, radioRelay[4], &RadioRelay::sendSNMPRequestsUintBsC);
   connect(this, &BackControl::sendSignalRequestBsC, radioRelay[4], &RadioRelay::sendRequestC);

   connect(this, &BackControl::sendSNMPRequestIntBsD, radioRelay[5], &RadioRelay::sendSNMPRequestsIntBsD);
   connect(this, &BackControl::sendSNMPRequestUintBsD, radioRelay[5], &RadioRelay::sendSNMPRequestsUintBsD);
   connect(this, &BackControl::sendSignalRequestBsD, radioRelay[5], &RadioRelay::sendRequestD);

       emit signalConnectTrF(TrF);
       emit signalConnectTrB(TrB);
       emit signalConnectBsA(Bs1);
       emit signalConnectBsB(Bs2);
       emit signalConnectBsC(Bs3);
       emit signalConnectBsD(Bs4);
   QTimer *timer = new QTimer(this);
   timer->setInterval(1000);
   timer->setSingleShot(true);
   timer->start();
   connect(timer, &QTimer::timeout, [=] {
       if (scenario_ == -1) {
           scenario_ = 0;
           applyScenario(scenario_);
       }
   });
}

void BackControl::sendSignalQualitySlot()
{
    emit sendSignalRequestBsA("1.3.6.1.4.1.50866.1.2.21.0");
    emit sendSignalRequestBsB("1.3.6.1.4.1.50866.1.2.21.0");
    emit sendSignalRequestBsC("1.3.6.1.4.1.50866.1.2.21.0");
    emit sendSignalRequestBsD("1.3.6.1.4.1.50866.1.2.21.0");
}


void BackControl::connectToRadioRelay()
{
//    emit signalConnectTrF(TrF);
//    emit signalConnectTrB(TrB);
//    emit signalConnectBsA(Bs1);
//    emit signalConnectBsB(Bs2);
//    emit signalConnectBsC(Bs3);
//    emit signalConnectBsD(Bs4);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &BackControl::sendSignalQualitySlot);
    timer->setInterval(500);
    timer->start();
}

void BackControl::connectToAttenuators()
{
    DWORD numOfDevices = 0;
    HID_UART_DEVICE_STR serialStr;
    HidUart_GetNumDevices(&numOfDevices, VID, PID);
    for (int i = 0; i < numOfDevices; i++) {
        HidUart_GetString(i, VID, PID, serialStr, HID_UART_GET_SERIAL_STR);
        fixedMap.insert(i, serialStr);
    }

    for (int i = 0; i < nAttBoxes; i++) {
        attenuators.push_back(new Attenuator(fixedMap.value(i), i, this));
        connect(this, &BackControl::connectToAttenuatorsSignal, attenuators[i], &Attenuator::connectToAttenuator);
        connect(this, &BackControl::sendSpeed, attenuators[i], &Attenuator::setSpeed);
        connect(attenuators[i], &Attenuator::connected, this, &BackControl::sendAttenuatorsName);
        connect(this, &BackControl::pauseSignal, attenuators[i], &Attenuator::pause);
    }
    connect(this, &BackControl::startAttenuator, attenuators[0], &Attenuator::startAttenuators);
    connect(this, &BackControl::startSecondAttenuator, attenuators[1], &Attenuator::startSecondAttenuator);
    connect(this, &BackControl::startFirstAttEnd, attenuators[0], &Attenuator::startFirstChannelEnd);
    connect(this, &BackControl::startThirdAgain, attenuators[1], &Attenuator::startThirdChannelAgain);
    emit connectToAttenuatorsSignal();

}

void BackControl::startAttDecision(int number)
{
     if (number == 0)
        emit startAttenuator();
     else if (number == 3)
         emit startThirdAgain();
    else
        emit startSecondAttenuator();
}

void BackControl::getDistance(double distanceKm)
{
    distance_ = distanceKm * 1000;
    startControl();
}

void BackControl::changeBaseStation(int baseStation)
{
    currentBs_ = baseStation;
}

void BackControl::startControl()
{
   switch (currentBs_) {
        case 1: {
        // max1
            if ((distance_ <= sBs1Distance*1000) && (distance_ > sBs1Distance*500)) {
                  if (scenario_ == 0) {
                      scenario_ = 1;
                      applyScenario(scenario_);
                  }
              }
            // half max1
              else if  ((distance_ <= sBs1Distance*500) && (distance_ > 400)) {
                  if (scenario_ == 1) {
                      scenario_ = 2;
                      applyScenario(scenario_);
                  }
              }
            // length train
              else if ((distance_ <= 400) && (distance_ > 30)) {
                  if (scenario_ == 2) {
                      scenario_ = 3;
                      applyScenario(scenario_);
                  }
              }
            // bs1
              else if (distance_ < 30) {
                  if (scenario_ == 3) {
                      scenario_ = 4;
                      applyScenario(scenario_);
                  }
              }
        }
        break;
    case 2: {
            // max2
          if  ((distance_ <= bs1Bs2Distance*1000) && (distance_ > bs1Bs2Distance*500)) {
              if (scenario_ == 4)  {
                  scenario_ = 5;
                  applyScenario(scenario_);
              }
          }
          // half max2
          else if ((distance_ <= bs1Bs2Distance*500) && (distance_ > 400)) {
              if (scenario_ == 5) {
                  scenario_ = 6;
                  applyScenario(scenario_);
              }
          }
          // length train
          else if ((distance_ <= 400) && (distance_ > 30)) {
              if (scenario_ == 6) {
                  scenario_ = 7;
                  applyScenario(scenario_);
              }
          }
          // base2
          else if (distance_ <= 30)  {
              if (scenario_ == 7) {
                  scenario_ = 8;
                  applyScenario(scenario_);
              }
          }
    }
    break;
    case 3: {
          // max3
          if  ((distance_ <= bs2Bs3Distance*1000) && (distance_ > bs2Bs3Distance*500)) {
              if (scenario_ == 8) {
                  scenario_ = 9;
                  applyScenario(scenario_);
              }
          }
          // half max3
          else if ((distance_ <= bs2Bs3Distance*500) && (distance_ > 400)) {
              if (scenario_ == 9) {
                  scenario_ = 10;
                  applyScenario(scenario_);
              }
          }
          //length train
          else if ((distance_ <= 400) && (distance_ > 30)) {
              if (scenario_ == 10) {
                  scenario_ = 11;
                  applyScenario(scenario_);
              }
          }
          // base3
          else if ((distance_ <= 30)) {
              if (scenario_ == 11) {
                  scenario_ = 12;
                  applyScenario(scenario_);
              }
          }
    }
    break;
    case 4: {
          // max4
          if  ((distance_ <= bs3bs4Distance*1000) && (distance_ > bs3bs4Distance*500)) {
              if (scenario_ == 12) {
                  scenario_ = 13;
                  applyScenario(scenario_);
              }
          }
          // half max4
          else if ((distance_ <= bs3bs4Distance*500) && (distance_ > 400)) {
              if (scenario_ == 13) {
                  scenario_ = 14;
                  applyScenario(scenario_);
              }
          }
          // train length
          else if ((distance_ <= 400) && (distance_ > 30)) {
              if (scenario_ == 14) {
                  scenario_ = 15;
                  applyScenario(scenario_);
              }
          }
          // base4
          else if ((distance_ <= 30)) {
              if (scenario_ == 15) {
                  scenario_ = 16;
                  applyScenario(scenario_);
              }
              scenario_ = 0;
              applyScenario(scenario_);
          }
    }
    break;
    case 5:
        return;
    }
}

void BackControl::applyScenario(int scenario)
{
    qDebug() << scenario << "Scenario";
    switch (scenario) {
    case 0:
        // init condition
        sendScenarioParamsTrain(1, 73000000, 0, 75000000);
        sendScenarioParamsBases(1, 1, 0, 0);
        if (ifBs1_)
            emit sendSNMPRequestUintBsA(FrTxOid, 83000000);
        if (ifBs2_)
            emit sendSNMPRequestUintBsB(FrTxOid, 85000000);
        if (ifBs3_)
            emit sendSNMPRequestUintBsC(FrTxOid, 83000000);
        if (ifBs4_)
            emit sendSNMPRequestUintBsD(FrTxOid, 85000000);
        break;
    case 1:
        // max1
        sendScenarioParamsTrain(1, 73000000, 0, 75000000);
        break;
    case 2:
        // half max1
        sendScenarioParamsTrain(0, 75000000, 1, 73000000);
        break;
    case 3:
        // length train to base1
        sendScenarioParamsTrain(1, 75000000, 1, 73000000);
        break;
    case 4:
        // base1
        sendScenarioParamsTrain(1, 75000000, 0, 73000000);
        break;
    case 5:
        // max2
        sendScenarioParamsTrain(1, 75000000, 0, 73000000);
        sendScenarioParamsBases(0, 1, 1, 0);
        break;
    case 6:
        // half max 2
        sendScenarioParamsTrain(0, 73000000, 1, 75000000);
        break;
    case 7:
        // length train to base2
        sendScenarioParamsTrain(1, 73000000, 1, 75000000);
        break;
    case 8:
        // base2
        sendScenarioParamsTrain(1, 73000000, 0, 75000000);
        break;
    case 9:
        // max3
        sendScenarioParamsTrain(1, 73000000, 0, 75000000);
        sendScenarioParamsBases(0, 0, 1, 1);
        break;
    case 10:
        // half max3
        sendScenarioParamsTrain(0, 75000000, 1, 73000000);
        break;
    case 11:
        // length train to base3
        sendScenarioParamsTrain(1, 75000000, 1, 73000000);
        break;
    case 12:
        // base3
        sendScenarioParamsTrain(1, 75000000, 0, 73000000);
        break;
    case 13:
        // max4
        sendScenarioParamsTrain(1, 75000000, 0, 73000000);
        sendScenarioParamsBases(1, 0, 0, 1);
        break;
    case 14:
        // half max4
        sendScenarioParamsTrain(0, 73000000, 1, 75000000);
        break;
    case 15:
        // length train to base4
        sendScenarioParamsTrain(1, 73000000, 1, 75000000);
        break;
    case 16:
        // base4
        sendScenarioParamsTrain(1, 73000000, 0, 75000000);
        break;
    default:
        break;
    }
}

void BackControl::sendScenarioParamsTrain(int txFront, unsigned long freqFront, int txBack, unsigned long freqBack)
{
        if (ifTrFront_) {
            emit sendSNMPRequestIntTrF(TxOnOid, txFront);
            emit sendSNMPRequestUintTrF(FrTxOid, freqFront);
        }
        if (ifTrBack_) {
            emit sendSNMPRequestIntTrB(TxOnOid, txBack);
            emit sendSNMPRequestUintTrB(FrTxOid, freqBack);
        }


}

void BackControl::sendScenarioParamsBases(int txBs1, int txBs2, int txBs3, int txBs4)
{
    if (ifBs1_)
        emit sendSNMPRequestIntBsA(TxOnOid, txBs1);
    if (ifBs2_)
        emit sendSNMPRequestIntBsB(TxOnOid, txBs2);
    if (ifBs3_)
        emit sendSNMPRequestIntBsC(TxOnOid, txBs3);
    if (ifBs4_)
        emit sendSNMPRequestIntBsD(TxOnOid, txBs4);

}

void BackControl::ifConnected(QString name, bool ifconnected)
{
    if (name == TrF)
        ifTrFront_ = ifconnected;
    else if (name == TrB)
        ifTrBack_ = ifconnected;
    else if (name == Bs1)
        ifBs1_ = ifconnected;
    else if (name == Bs2)
        ifBs2_ = ifconnected;
    else if (name == Bs3)
        ifBs3_ = ifconnected;
    else if (name == Bs4)
        ifBs4_ = ifconnected;
}
