#ifndef SNMPMANAGEMENT_H
#define SNMPMANAGEMENT_H

#include <QObject>
#include <QTimer>
#include <QTime>
#include <QFile>
#include <QDebug>
#include <snmp_pp/snmp_pp.h>

using namespace Snmp_pp;

class SNMPManagement : public QObject
{
    Q_OBJECT
    QTimer *getTime;
    Snmp *snmp;
    SnmpTarget *target;
    CTarget ctarget;
    unsigned int timeout;
    bool initializeTarget;

protected:
    UdpAddress address;
    unsigned short port;
    int interval;
    QByteArray readCommunityName, writeCommunityName;
    bool connectStat;

public:
    explicit SNMPManagement(QObject *parent = 0);
    bool setSnmp(const QString& oidstr,const int &value);
    bool setSnmpUnsigned(const QString &oidstr, const unsigned long &value);
    QStringList getSnmp(QStringList oidstr);
    QString getSnmp(const QString& oidstr);
    bool initializeSnmp();
    ~SNMPManagement();

signals:
    void error(QString error);
    void disconnected();
    void connected();
    void startTimer();
    void stopTimer();


public slots:
    void start();
    virtual void getStat(){}
    void disconnectFromSnmp();
    bool isConnected(){ qDebug() << "conStat" << connectStat; return connectStat;}
};

#endif // SNMPMANAGEMENT_H
