#ifndef ATTENUATOR_H
#define ATTENUATOR_H

#include <QDebug>
#include <QMap>
#include <QObject>
#include <QTimer>
#include <windows.h>
#include "SLABCP2110.h"

class Attenuator : public QObject
{
    Q_OBJECT
     const WORD VID = 0x10C4;
     const WORD PID = 0xEA80;
     const DWORD baudRate = 115200;
     const BYTE dataBits = HID_UART_EIGHT_DATA_BITS;
     const BYTE parity =  HID_UART_NO_PARITY;
     const BYTE stopBits = HID_UART_SHORT_STOP_BIT;
     const BYTE flowControl = HID_UART_NO_FLOW_CONTROL;
     double startBs1 = 960;
     double bs1Bs2 = 1148;
     double bs2Bs3 = 1224;
     double bs3Bs4 = 1100;
     double bs1bs2Normal = 1148;

     double sBs1Distance = 1.91422;
     double bs1Bs2Distance = 2.04017;
     double bs1Bs2DistanceNormal = 2.04017;
     double bs2Bs3Distance = 1.97918;
     double bs3bs4Distance = 1.98986;

     QMap<int, QString> fixedMap;
     enum class Direction {
        up, down
     };

    Direction dir;
    bool ifFirst = false, ifSecond = false, ifFirstEnd = false, if2ndStopped = false, if4thStopped = false, ifStarted4th = false;
    BYTE lastByte1_2nd, lastByte2_2nd, maxByte1, maxByte2, minByte1, minByte2, devAddrByte, lastByte1_1st, lastByte2_1st, lastByte1_3rd, lastByte2_3rd, lastByte1_4th, lastByte2_4th;
    HID_UART_DEVICE devHandle;
    QMap<int, QString> devices;
    QString initInformation, attenuatorName;
    QString serialNumber;
    QTimer *firstChannelTimer, *secondChannelTimer, *thirdChannelTimer, *forthChannelTimer, *firstEndTimer;
    int deviceIndex;
    int minValue, maxValue, curValue, codeFreqDiap, responseCode;
    bool connectionState;
    void writeCommand(BYTE devAddr, BYTE channelNum);
    void writeCommandThird(BYTE devAddr, BYTE channelNum);
    void writeCommandMinMax(BYTE devAddr, BYTE channelNum, int minmax);

public:
    explicit Attenuator(QString serialNumber, int deviceIndex, QObject *parent = 0);

signals:
    void sendSerialString(QString str);
    void connected(QString str);

public slots:
    void setSpeed(double speed);
    void connectToAttenuator();
    void startAttenuators();
    void startSecondAttenuator();
    void startFirstChannelEnd();
    void startThirdChannelAgain();
    void pause();

private:
    void reduceAtt(int which);
    void escalateFirstChannel();
    void escalateSecondChannel();
    void escalateThirdChannel();
    void escalateForthChannel();
    void escalateFirstEndSlot();
    void escalateThirdChannelAgain();
    void getMinMaxDb(int min, int max);
};

#endif // ATTENUATOR_H
