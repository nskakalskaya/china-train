#ifndef RADIORELAY_H
#define RADIORELAY_H

#include <QObject>
#include <QByteArray>
#include <QThread>
#include "snmpmanagement.h"

struct RRCInfo {
    unsigned int numPortOnDlink, numRadio;
    QString tempSensor1, tempSensor2;
    QString ATT;
    QString uplink, downlink;
    QString RXPacketsPerSec, TXPacketsPerSec;
    QString MSE;
    QString UAG,UMIN12V,U5V,U5VSB,URE;
    QString consumption;
    QString time;
    bool gigabyteModel;
    bool radioStatus;
    bool heaterStatus;
};

class RadioRelay : public SNMPManagement
{
    Q_OBJECT
    QString ipaddr;
    unsigned int numPort;
    void sendSNMPRequests();
    QThread logThread;

public:
    RadioRelay(QObject *parent = 0);

signals:
    void isSNMPConnected(QString ipaddr, bool connected);
    void writeLog(QString what);
    void sendBackSignalQuality(QString res, int index);

public slots:
    void connectToSnmpTrF(QString ipaddr);
    void connectToSnmpTrB(QString ipaddr);
    void connectToSnmpBsA(QString ipaddr);
    void connectToSnmpBsB(QString ipaddr);
    void connectToSnmpBsC(QString ipaddr);
    void connectToSnmpBsD(QString ipaddr);

    void sendSNMPRequestsIntTrF(QString OID, int value);
    void sendSNMPRequestsUintTrF(QString OID, unsigned long value);

    void sendSNMPRequestsIntTrB(QString OID, int value);
    void sendSNMPRequestsUintTrB(QString OID, unsigned long value);

    void sendSNMPRequestsIntBsA(QString OID, int value);
    void sendSNMPRequestsUintBsA(QString OID, unsigned long value);
    void sendRequestA(QString OID);

    void sendSNMPRequestsIntBsB(QString OID, int value);
    void sendSNMPRequestsUintBsB(QString OID, unsigned long value);
    void sendRequestB(QString OID);

    void sendSNMPRequestsIntBsC(QString OID, int value);
    void sendSNMPRequestsUintBsC(QString OID, unsigned long value);
    void sendRequestC(QString OID);

    void sendSNMPRequestsIntBsD(QString OID, int value);
    void sendSNMPRequestsUintBsD(QString OID, unsigned long value);
    void sendRequestD(QString OID);

    bool ifConnected();


};

#endif // RADIORELAY_H
