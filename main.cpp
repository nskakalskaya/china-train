#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtPositioning/private/qgeoprojection_p.h>
#include "train.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Train train;
    qmlRegisterType<Train>("io.qt.train", 1, 0, "Train");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("Train", &train);
    engine.load(QUrl(QStringLiteral("qrc:/ChinaMap.qml")));
    return app.exec();
}




