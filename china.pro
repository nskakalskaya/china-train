TEMPLATE = app
TARGET = ChinaMap
QT += qml quick positioning positioning-private location

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp \
    train.cpp \
    snmpmanagement.cpp \
    backcontrol.cpp \
    radiorelay.cpp \
    attenuator.cpp \
    config.cpp

RESOURCES += qml.qrc

#target.path = $$[QT_INSTALL_EXAMPLES]/location/planespotter
#INSTALLS += target

HEADERS += \
    train.h \
    snmpmanagement.h \
    radiorelay.h \
    backcontrol.h \
    attenuator.h \
    config.h

win32: LIBS += -L$$PWD/../../_myLibs/snmpp/lib/ -lsnmp++ -lws2_32
INCLUDEPATH += $$PWD/../../_myLibs/snmpp/include
DEPENDPATH += $$PWD/../../_myLibs/snmpp/include
win32:win32-g++: PRE_TARGETDEPS += $$PWD/../../_myLibs/snmpp/lib/libsnmp++.a

# SiLabs HID to UART library
LIBS += -L$$PWD/../../_myLibs/siusbcp211x/lib/ -lSLABHIDtoUART
INCLUDEPATH += $$PWD/../../_myLibs/siusbcp211x/include
DEPENDPATH += $$PWD/../../_myLibs/siusbcp211x/include
