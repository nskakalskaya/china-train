#include "config.h"

Config::Config(QObject *parent) : QObject(parent)
{
    cnf = new QSettings("config.ini", QSettings::IniFormat);
    QFileInfo file("config.ini");
    if (file.exists()) {
        deltaDistance = cnf->value("delta_distance").toString();
        deltaTrain = cnf->value("delta_train").toString();
    }
    else {
        deltaDistance = "20";
        deltaTrain = "0";
    }
}

QString Config::getDeltaDistance()
{
    return deltaDistance;
}

QString Config::getDeltaTrain()
{
    return deltaTrain;
}
