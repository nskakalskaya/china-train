#ifndef TRAIN_H
#define TRAIN_H

#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QObject>
#include <QTime>
#include <QThread>
#include <QBasicTimer>
#include <QDebug>
#include <QEasingCurve>
#include <QGeoCoordinate>
#include <QQuickView>
#include <QWidget>
#include <QStringList>
#include <QQmlComponent>
#include <QtPositioning/private/qgeoprojection_p.h>
#include "backcontrol.h"
#include "config.h"

#define earthRadiusKm 6371.0

class Train : public QObject
{
    Q_OBJECT
//    QGeoCoordinate startbs = QGeoCoordinate(39.634074, 116.817308);
    QGeoCoordinate startbs = QGeoCoordinate(39.621114, 116.825481);
    QGeoCoordinate bs1 = QGeoCoordinate(39.605642, 116.835279);
    QGeoCoordinate bs2 = QGeoCoordinate(39.589060, 116.845471);
    QGeoCoordinate bs3 = QGeoCoordinate(39.572992, 116.855406);
    QGeoCoordinate bs4 = QGeoCoordinate(39.556824, 116.865356);

    Q_PROPERTY(QGeoCoordinate position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(QGeoCoordinate from READ from WRITE setFrom NOTIFY fromChanged)
    Q_PROPERTY(QGeoCoordinate to READ to WRITE setTo NOTIFY toChanged)
    Q_PROPERTY(QGeoCoordinate calcPos READ calcPos WRITE setCalcPos NOTIFY calcPosChanged)
    Q_PROPERTY(int duration READ duration WRITE setDuration NOTIFY durationChanged)
    Q_PROPERTY(double speed READ speed WRITE setSpeed NOTIFY speedChanged)
    Q_PROPERTY(int finishFlag READ finishFlag WRITE setFinishFlag NOTIFY finishFlagChanged)

    QGeoCoordinate fromCoordinate, toCoordinate;
    QBasicTimer timer;
    QTime startTime, finishTime;
    QEasingCurve easingCurve;

    BackControl *fullController;
    QThread controlThread;
    Config config;

    bool ifFirstAttWasEnabled{false}, ifSecondAttWasEnabled = false, ifTimerWasStopped = false, ifSavedTime = false, ifFirstAttEnd = false, ifThirdWasEnabled = false;
    int ifFinish{0}, currentBSint{1}, x{1};
    double deltaTrain;

    void checkIfEnded();
    void updatePosition();
    double distanceEarth(QGeoCoordinate &coordinates, QGeoCoordinate &bs);


public:
    explicit Train(QObject *parent = 0);
    ~Train();

    QGeoCoordinate currentPosition, toCalcPos;
    QStringList uarts_;
    QString curBS, quality1_, quality2_, quality3_, quality4_;
    QTimer *checkTimer;
    int dur;
    double spd, distanceToBS;

    void setFrom(const QGeoCoordinate& from) {
        fromCoordinate = from;
    }
    QGeoCoordinate from() const {
        return fromCoordinate;
    }

    void setTo(const QGeoCoordinate& to) {
        toCoordinate = to;
    }
    QGeoCoordinate to() const {
        return toCoordinate;
    }


public slots:
    Q_INVOKABLE bool isRunning() const {
        return timer.isActive();
    }

    Q_INVOKABLE bool ifStopped() const {
        return ifTimerWasStopped;
    }

    Q_INVOKABLE QString currentBS() const {
        return curBS;
    }

    Q_INVOKABLE double distanceLeft() const {
        return distanceToBS;
    }

    Q_INVOKABLE QStringList uarts() const {
        return uarts_;
    }

    Q_INVOKABLE QString quality1() const {
        return quality1_;
    }

    Q_INVOKABLE QString quality2() const {
        return quality2_;
    }

    Q_INVOKABLE QString quality3() const {
        return quality3_;
    }

    Q_INVOKABLE QString quality4() const {
        return quality4_;
    }

    void setSpeed(const double &c);
    double speed() const;

    void setDuration(const int &c);
    int duration() const;

    void setCalcPos(const QGeoCoordinate &c);
    QGeoCoordinate calcPos() const;

    void setFinishFlag(const int &c);
    int finishFlag() const;

    void setPosition(const QGeoCoordinate &c);
    QGeoCoordinate position() const;

    void startTrain();
    void doPause();
    void getCurrentBs(int baseStation);
    void getAttenuatorsName(QString str);
    void getSignalQualities(QString string, int index);
    void countForwardDistance(QGeoCoordinate &coords);

signals:
    void controlRelays();
    void durationChanged();
    void startFirstAttEnd();
    void finishFlagChanged();
    void doPauseSignal();
    void positionChanged();
    void arrived();
    void departed();
    void toChanged();
    void fromChanged();
    void calcPosChanged();
    void snmpChanged();
    void startChanged();

    void sendDistance(double distance);
    void speedChanged(double spd);
    void sendCurrentBS(int bs);

    void startAttenuators(int number);

protected:
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE
    {
        if (!event)
            return;
        if (event->timerId() == timer.timerId())
            updatePosition();
        else
            QObject::timerEvent(event);
    }

};

#endif
