#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>

#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QObject>
#include <QTime>
#include <QBasicTimer>
#include <QDebug>
#include <QEasingCurve>
#include <QGeoCoordinate>
#include <QQuickView>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include "train.h"
#include <QtPositioning/private/qgeoprojection_p.h>

class MainWindow : public QObject
{
    Q_OBJECT
public:
    explicit MainWindow(QObject *parent = 0);

signals:

public slots:
};

#endif // MAINWINDOW_H
