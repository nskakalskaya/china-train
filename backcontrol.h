#ifndef BACKCONTROL_H
#define BACKCONTROL_H

#include <QObject>
#include <QThread>
#include "radiorelay.h"
#include "attenuator.h"

class BackControl : public QObject
{
    Q_OBJECT
    const static int nRelays = 6;
    const static int nAttBoxes = 2;
    const static WORD VID = 0x10C4;
    const static WORD PID = 0xEA80;
    constexpr static double sBs1Distance = 1.91422;
    constexpr static double bs1Bs2Distance = 2.04017;
    constexpr static double bs2Bs3Distance = 1.97918;
    constexpr static double bs3bs4Distance = 1.98986;
    bool ifTrFront_, ifTrBack_, ifBs1_, ifBs2_, ifBs3_, ifBs4_;

    QVector <RadioRelay * >radioRelay;
    QThread radioThread[nRelays];

    QVector <Attenuator *> attenuators;
    QMap<int, QString> fixedMap;
    QTimer *timer;

    QString TrF = "192.168.10.243";
    QString TrB = "192.168.10.244";
    QString Bs1 = "192.168.10.231";
    QString Bs2 = "192.168.10.232";
    QString Bs3 = "192.168.10.233";
    QString Bs4 = "192.168.10.234";
    QString TxOnOid = "1.3.6.1.4.1.50866.1.2.6.0";
    QString FrTxOid = "1.3.6.1.4.1.50866.1.2.13.0" ;

    int currentBs_ = 1, scenario_ = -1;
    double distance_;

public:
    explicit BackControl(bool ifTrFront, bool ifTrBack, bool ifBs1, bool ifBs2, bool ifBs3, bool ifBs4, QObject *parent = 0);

signals:
    void startThirdAgain();
    void sendSpeed(double speed);
    void pauseSignal();
    void signalConnectTrF(QString ipaddr);
    void signalConnectTrB(QString ipaddr);
    void signalConnectBsA(QString ipaddr);
    void signalConnectBsB(QString ipaddr);
    void signalConnectBsC(QString ipaddr);
    void signalConnectBsD(QString ipaddr);
    void connectToAttenuatorsSignal();
    void startAttenuator();
    void startSecondAttenuator();
    void startFirstAttEnd();

    void sendAttenuatorsName(QString serialString);
    void sendSignalQuality(QString send, int index);

    void sendSNMPRequestIntTrF(QString Oid, int value);
    void sendSNMPRequestUintTrF(QString Oid, unsigned long value);

    void sendSNMPRequestIntTrB(QString Oid, int value);
    void sendSNMPRequestUintTrB(QString Oid, unsigned long value);

    void sendSNMPRequestIntBsA(QString Oid, int value);
    void sendSNMPRequestUintBsA(QString Oid, unsigned long value);
    void sendSignalRequestBsA(QString Oid);

    void sendSNMPRequestIntBsB(QString Oid, int value);
    void sendSNMPRequestUintBsB(QString Oid, unsigned long value);
    void sendSignalRequestBsB(QString Oid);

    void sendSNMPRequestIntBsC(QString Oid, int value);
    void sendSNMPRequestUintBsC(QString Oid, unsigned long value);
    void sendSignalRequestBsC(QString Oid);

    void sendSNMPRequestIntBsD(QString Oid, int value);
    void sendSNMPRequestUintBsD(QString Oid, unsigned long value);
    void sendSignalRequestBsD(QString Oid);

    void changeBaseStationSignal(int baseStation);
    void sendScenario(int scenario);

public slots:
     void sendSignalQualitySlot();
     void connectToRadioRelay();
     void connectToAttenuators();
     void startAttDecision(int number);
     void getDistance(double distanceKm);
     void changeBaseStation(int baseStation);
     void startControl();

private:
     void applyScenario(int scenario);
     void sendScenarioParamsTrain(int txFront, unsigned long freqFront, int txBack, unsigned long freqBack);
     void sendScenarioParamsBases(int txBs1, int txBs2, int txBs3, int txBs4);
     void ifConnected(QString name, bool ifconnected);
};

#endif // BACKCONTROL_H
