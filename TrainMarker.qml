import QtQuick 2.4
import QtLocation 5.6
import QtPositioning 5.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

MapQuickItem {
    id: train
    property string trainName;
    property int bearing: 0;
    anchorPoint.x: image.width/2
    anchorPoint.y: image.height/2
    sourceItem: Grid {
        columns: 1
        Grid {
            horizontalItemAlignment: Grid.AlignHCenter
            Image {
                id: image
                rotation: bearing
                source: "qrc:/icons/marker.png"
            }
            Rectangle {
                id: bubble
                color: "lightblue"
                border.width: 1
                width: text.width * 1.3
                height: text.height * 1.3
                radius: 5
                Text {
                    id: text
                    anchors.centerIn: parent
                    //text: trainName
                }
            }
        }
   }

}


