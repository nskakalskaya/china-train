#include "train.h"

Train::Train(QObject *parent) {

   fullController = new BackControl(false, false, false, false, false, false);
   fullController->moveToThread(&controlThread);
   controlThread.start();
   connect(this, &Train::doPauseSignal, fullController, &BackControl::pauseSignal);
   connect(fullController, &BackControl::sendAttenuatorsName, this, &Train::getAttenuatorsName);
   connect(fullController, &BackControl::sendSignalQuality, this, &Train::getSignalQualities);
   connect(this, &Train::snmpChanged, fullController, &BackControl::connectToAttenuators);
   connect(this, &Train::snmpChanged, fullController, &BackControl::connectToRadioRelay);
   emit snmpChanged();
   deltaTrain = config.getDeltaTrain().toDouble();
   connect(this, &Train::startAttenuators, fullController, &BackControl::startAttDecision);
   connect(this, &Train::startFirstAttEnd, fullController, &BackControl::startFirstAttEnd);
   connect(this, &Train::sendCurrentBS, fullController, &BackControl::changeBaseStation);
   connect(this, &Train::sendDistance, fullController, &BackControl::getDistance);
   connect(this, &Train::controlRelays, fullController, &BackControl::startControl);
   connect(this, &Train::speedChanged, fullController, &BackControl::sendSpeed);
   checkTimer = new QTimer(this);
   checkTimer->setInterval(0);
   connect(checkTimer, &QTimer::timeout, this, &Train::checkIfEnded);
   checkTimer->start();
}

Train::~Train() {
    controlThread.quit();
    controlThread.wait();
}

void Train::startTrain() {
    startTime = QTime::currentTime();
    finishTime = startTime.addMSecs(dur);
    timer.start(15, this);
    emit departed();
}

void Train::doPause() {
    if (!ifTimerWasStopped) {
        emit doPauseSignal();
        timer.stop();
        ifTimerWasStopped = true;
    }
    else {
        timer.start(15, this);
        ifTimerWasStopped = false;
    }
}


void Train::setPosition(const QGeoCoordinate &c) {
    if (currentPosition == c)
        return;
    currentPosition = c;
    emit positionChanged();
}

QGeoCoordinate Train::position() const
{
    return currentPosition;
}

void Train::setSpeed(const double &c)
{
    if (spd == c)
        return;
    spd = c;
    emit speedChanged(spd);
}

double Train::speed() const
{
    return spd;
}

void Train::setDuration(const int &c)
{
    if (dur == c)
        return;
    dur = c;
    easingCurve.setType(QEasingCurve::Linear);
    easingCurve.setPeriod(dur);
    emit durationChanged();
}

int Train::duration() const
{
    return dur;

}

void Train::setCalcPos(const QGeoCoordinate &c)
{
    if (toCalcPos == c)
        return;
    toCalcPos = c;
    countForwardDistance(toCalcPos);
    emit calcPosChanged();
    countForwardDistance(toCalcPos);
}

QGeoCoordinate Train::calcPos() const
{
    return toCalcPos;
}

void Train::setFinishFlag(const int &c)
{
    ifFinish = c;
}

int Train::finishFlag() const
{
    return ifFinish;
}

void Train::getCurrentBs(int baseStation)
{
    currentBSint = baseStation;
}

void Train::getAttenuatorsName(QString str)
{
    uarts_.append(str);
}

void Train::getSignalQualities(QString string, int index)
{

    switch (index) {
    case 1:
        if (string.toInt() == 0)
            quality1_ = "bad";
        else
            quality1_ = "good";
        break;
    case 2:
        if (string.toInt() == 0)
            quality2_ = "bad";
        else
            quality2_ = "good";
        break;
    case 3:
        if (string.toInt() == 0)
            quality3_ = "bad";
        else
            quality3_ = "good";
        break;
    case 4:
        if (string.toInt() == 0)
            quality4_ = "bad";
        else
            quality4_ = "good";
        break;
    }
}


void Train::countForwardDistance(QGeoCoordinate &coords)
{
    QGeoCoordinate selectedBS;
    switch(x) {
    case 1:
        if (!ifFirstAttWasEnabled)
            emit startAttenuators(0);
        ifFirstAttWasEnabled = true;
        ifFirstAttEnd = false;
        selectedBS = bs1;
        currentBSint = 1;
        curBS = "BS1";
        break;
    case 2:
        if (!ifSecondAttWasEnabled)
            emit startAttenuators(1);
        ifSecondAttWasEnabled = true;
        selectedBS = bs2;
        currentBSint = 2;
        curBS = "BS2";
        break;
    case 3:
        if (!ifThirdWasEnabled)
            emit startAttenuators(3);
        ifThirdWasEnabled = true;
        selectedBS = bs3;
        currentBSint = 3;
        curBS = "BS3";
        break;
    case 4:
        if (!ifFirstAttEnd)
            emit startFirstAttEnd();
        ifFirstAttEnd = true;
        selectedBS = bs4;
        currentBSint = 4;
        curBS = "BS4";
        ifFirstAttWasEnabled = false;
        ifSecondAttWasEnabled = false;
        ifThirdWasEnabled = false;
        break;
    }
    double distance = distanceEarth(coords, selectedBS) ;
    if (distance < 0.03)
        x++;

    emit sendCurrentBS(currentBSint);
    distanceToBS = distance;
    emit sendDistance(distance);
}

double Train::distanceEarth(QGeoCoordinate &coordinates, QGeoCoordinate &bs)
{
    double latitudeTrain = coordinates.latitude();
    double longitudeTrain = coordinates.longitude();
    double latitudeBase = bs.latitude();
    double longitudeBase = bs.longitude();

    auto lambdaLat1 = [] (double latitudeTrain) {return latitudeTrain * M_PI / 180; };
    auto lambdaLong1 = [] (double longitudeTrain) {return longitudeTrain * M_PI / 180; };
    auto lambdaLat2 = [] (double latitudeBase)  {return latitudeBase * M_PI / 180; };
    auto lambdaLong2 = [] (double longitudeBase) {return longitudeBase * M_PI / 180; };

    double lat1 = lambdaLat1(latitudeTrain);
    double long1 = lambdaLong1(longitudeTrain);
    double lat2 = lambdaLat2(latitudeBase);
    double long2 = lambdaLong2(longitudeBase);

    double u = sin((lat2 - lat1)/2);
    double v = sin((long2 - long1)/2);

    return 2.0 * earthRadiusKm * asin(sqrt(u * u + cos(lat1) * cos(lat2) * v * v));
}

void Train::checkIfEnded()
{
    if (ifFinish == 2) {
        x = 1;
        currentBSint = 1;
        ifFinish = 0;
    }
}

void Train::updatePosition()
{
    if (timer.isActive()) {
        qreal progress;
        QTime current = QTime::currentTime();
        if (current >= finishTime) {
            progress = 1.0;
            timer.stop();
        } else
            progress = ((qreal)startTime.msecsTo(current) / dur);

        setPosition(QGeoProjection::coordinateInterpolation(fromCoordinate, toCoordinate, easingCurve.valueForProgress(progress)));
    }
    if (!timer.isActive())
        emit arrived();
}

